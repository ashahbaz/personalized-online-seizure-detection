import os
import sys


def main():
    """Main function to update the models overtime.
    Usage:
    python train.py -q
    """
    if "--quiet" in sys.argv or "-q" in sys.argv:

        class Quiet:
            def write(self, *args, **kwargs):
                pass

        sys.stdout = Quiet()  # Prevents the script from printing

    # Training global Models
    path = "training_scripts/global.py"
    # for i in range(1, 4):
    for i in range(1, 2):
        args = ["--global_group", f"{i}"]
        os.system(f"python {path} {' '.join(args)}")

    # Training cl Models
    # for i in range(1, 4):
    for i in range(1, 2):
        for seed in range(3):
            path = "training_scripts/cl.py"
            args = [
                "--exp_name",
                f"CL_Exp_Gr{i}_S{seed}",
                "--seed",
                f"{seed}",
                "--num_rec",
                "1",
                "--global_group",
                f"{i}",
            ]
            os.system(f"python {path} {' '.join(args)}")

    # Training base Models
    for i in range(1, 4):
        for seed in range(3):
            path = "training_scripts/base.py"
            args = [
                "--exp_name",
                f"Base_Exp_Gr{i}_S{seed}",
                "--seed",
                f"{seed}",
                "--num_rec",
                "2",
                "--global_group",
                f"{i}",
            ]
            os.system(f"python {path} {' '.join(args)}")

    # Training os Models
    for i in range(1, 4):
        for seed in range(3):
            path = "training_scripts/os.py"
            args = [
                "--exp_name",
                f"OS_Exp_Gr{i}_S{seed}",
                "--seed",
                f"{seed}",
                "--num_rec",
                "2",
                "--global_group",
                f"{i}",
            ]
            os.system(f"python {path} {' '.join(args)}")

    # Training hist Models
    for i in range(1, 4):
        for seed in range(3):
            path = "training_scripts/hist.py"
            args = [
                "--exp_name",
                f"Hist_Exp_Gr{i}_S{seed}",
                "--seed",
                f"{seed}",
                "--num_rec",
                "1",
                "--global_group",
                f"{i}",
            ]
            os.system(f"python {path} {' '.join(args)}")

    # Training boundary Models
    for i in range(1, 4):
        for seed in range(3):
            path = "training_scripts/boundary.py"
            args = [
                "--exp_name",
                f"Boundary_Exp_Gr{i}_S{seed}",
                "--seed",
                f"{seed}",
                "--num_rec",
                "1",
                "--global_group",
                f"{i}",
            ]
            os.system(f"python {path} {' '.join(args)}")


if __name__ == "__main__":
    main()
