import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))
import argparse as ap
import pickle
import random
import torch
import numpy as np
from torch.utils.data import DataLoader, ConcatDataset

from utils import FCN
from utils import fine_tune
from utils.chbmit import CHB_Store


def load_all_pat(all_patients, opt):
    def get_dataset(fld_dir):
        train_dir = os.path.join(fld_dir, "train.pkl")
        val_dir = os.path.join(fld_dir, "val.pkl")

        with open(train_dir, "rb") as file:
            train_dataset = pickle.load(file)
        with open(val_dir, "rb") as file:
            val_dataset = pickle.load(file)
        print("Loaded:\t", fld_dir)
        train_dataset = CHB_Store.CHBMIT.from_pkl(train_dataset)
        val_dataset = CHB_Store.CHBMIT.from_pkl(val_dataset)

        return train_dataset, val_dataset

    train_list = []
    val_list = []
    for pat in all_patients:
        pat_dir = os.path.join(opt.ds_dir, pat)
        fld_dirs = sorted(os.listdir(pat_dir), key=lambda x: int(x))
        fld_dirs = [os.path.join(pat_dir, item) for item in fld_dirs]
        # Adding other recordings
        train_dataset, val_dataset = get_dataset(fld_dirs[0])
        for rec_dir in fld_dirs[1:]:
            temp_train_dataset, temp_val_dataset = get_dataset(rec_dir)
            # Seizure part
            train_dataset.extend_dataset(
                *temp_train_dataset.get_all_seizures()
            )
            val_dataset.extend_dataset(*temp_val_dataset.get_all_seizures())
            # Non-Seizure part
            train_dataset.extend_dataset(
                *temp_train_dataset.get_all_Nonseizures()
            )
            val_dataset.extend_dataset(*temp_val_dataset.get_all_Nonseizures())

        train_dataset.oversample_seizures(64)
        # Balancing dataset
        train_dataset.repeat_seizures()
        train_list.append(train_dataset)
        val_list.append(val_dataset)

    train_dataset = ConcatDataset(train_list)
    val_dataset = ConcatDataset(val_list)
    train_loader = DataLoader(
        train_dataset, batch_size=opt.batch_size, shuffle=True
    )
    val_loader = DataLoader(
        val_dataset, batch_size=opt.batch_size, shuffle=True
    )
    return train_loader, val_loader


def train_global_model(all_patients, opt):
    """Train the global model for the given group of patients.
    Args:
    pat: str, patient name
    opt: argparse object
    """
    # This is used to update the Global model
    print("\n***********************************")
    print(f"Training the global model for Group {opt.global_group}\n\n")
    # Load the model
    model = FCN.Net(in_channels=18)
    model.to(opt.device)

    # Getting new loaders
    train_loader, val_loader = load_all_pat(all_patients, opt)

    # Fine-tuning the model
    save_path = opt.global_mdl_dir
    criterion = torch.nn.CrossEntropyLoss()
    criterion.to(opt.device)

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(
        optimizer, milestones=[50, 75], gamma=0.2
    )
    fine_tune(
        train_loader,
        val_loader,
        model,
        opt.epochs,
        save_path,
        criterion,
        optimizer,
        opt.device,
        scheduler,
    )


def main():
    """Main function to update the models overtime.
    Usage:
    python global.py --global_group 1
    """
    parser = ap.ArgumentParser()
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--batch_size", type=int, default=256)
    parser.add_argument(
        "--epochs",
        type=int,
        default=100,
        help="number of epochs to train (default: 100)",
    )
    parser.add_argument(
        "--global_group",
        type=int,
        required=True,
        default=1,
        help="name of the global group, 1, 2 or 3",
    )
    opt = parser.parse_args()

    cuda = torch.cuda.is_available()
    opt.device = torch.device("cuda:0" if cuda else "cpu")
    print("Device: {}".format(opt.device))

    torch.manual_seed(opt.seed)
    random.seed(opt.seed)
    os.environ["PYTHONHASHSEED"] = str(opt.seed)
    np.random.seed(opt.seed)

    if cuda:
        map_location = None
        torch.cuda.manual_seed(opt.seed)
        torch.cuda.manual_seed_all(opt.seed)
        torch.backends.cudnn.deterministic = True
    else:

        def map_location(storage, loc):
            return storage

    group_1 = [6, 21, 9, 7, 4, 23, 5, 8]
    group_2 = [2, 22, 20, 18, 3, 10, 11, 12]
    group_3 = [16, 14, 19, 17, 13, 1, 24, 15]

    if opt.global_group == 1:
        group_client = group_1
        global_mdl_dir = "Global_Gr1.pth"  # to store the output model
    elif opt.global_group == 2:
        group_client = group_2
        global_mdl_dir = "Global_Gr2.pth"  # to store the output model
    elif opt.global_group == 3:
        group_client = group_3
        global_mdl_dir = "Global_Gr3.pth"  # to store the output model
    else:
        raise ValueError("Invalid global group")

    opt.map_location = map_location
    opt.ds_dir = "tr_val_chunks"
    opt.global_mdl_dir = os.path.join(
        "experiments_out", "global_models", global_mdl_dir
    )

    all_patients = ["chb" + str(i).zfill(2) for i in group_client]

    train_global_model(all_patients, opt)


if __name__ == "__main__":
    main()
