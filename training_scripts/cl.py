import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))
import argparse as ap
import pickle
import random
import torch
import numpy as np
from torch.utils.data import DataLoader

from utils import FCN
from utils import fine_tune
from utils.MemoryManager import MemoryManager
from utils.chbmit import CHB_Store


def load_pat_ml_boundary(rec_dirs, mem_manager, opt):
    assert isinstance(rec_dirs, list), "fld_dir should be a list"

    def get_dataset(fld_dir):
        train_dir = os.path.join(fld_dir, "train.pkl")
        val_dir = os.path.join(fld_dir, "val.pkl")

        with open(train_dir, "rb") as file:
            train_dataset = pickle.load(file)
        with open(val_dir, "rb") as file:
            val_dataset = pickle.load(file)
        print("Loaded:\t", fld_dir)
        train_dataset = CHB_Store.CHBMIT.from_pkl(train_dataset)
        val_dataset = CHB_Store.CHBMIT.from_pkl(val_dataset)

        return train_dataset, val_dataset

    def split_train_val(info):
        total_elements = len(info[0])
        indices = np.random.permutation(total_elements)
        split_80 = int(0.8 * total_elements)
        train_info = [data[indices[:split_80]] for data in info]
        val_info = [data[indices[split_80:]] for data in info]
        return train_info, val_info

    train_dataset, val_dataset = get_dataset(rec_dirs[0])

    # Adding other recordings
    if len(rec_dirs) > 1:
        for rec_dir in rec_dirs[1:]:
            temp_train_dataset, temp_val_dataset = get_dataset(rec_dir)
            # Seizure part
            train_dataset.extend_dataset(
                *temp_train_dataset.get_all_seizures()
            )
            val_dataset.extend_dataset(*temp_val_dataset.get_all_seizures())
            # Non-Seizure part
            train_dataset.extend_dataset(
                *temp_train_dataset.get_all_Nonseizures()
            )
            val_dataset.extend_dataset(*temp_val_dataset.get_all_Nonseizures())

    if mem_manager.is_ready():
        sei_info, nonsei_info = mem_manager.get_data()

        # Randomly splitting the memory data
        train_sei_info, val_sei_info = split_train_val(sei_info)
        train_nonsei_info, val_nonsei_info = split_train_val(nonsei_info)

        train_dataset.extend_dataset(*train_sei_info)
        train_dataset.extend_dataset(*train_nonsei_info)
        val_dataset.extend_dataset(*val_sei_info)
        val_dataset.extend_dataset(*val_nonsei_info)

        print(f"Injected {len(sei_info[0])} Seizures.")
        print(f"Injected {len(nonsei_info[0])} Non-Seizures.")

    train_dataset.oversample_seizures(64)
    # Balancing dataset
    train_dataset.repeat_seizures()

    train_loader = DataLoader(
        train_dataset, batch_size=opt.batch_size, shuffle=True
    )
    val_loader = DataLoader(
        val_dataset, batch_size=opt.batch_size, shuffle=True
    )
    return train_loader, val_loader


def update_model(pat, opt):
    """Updating model function for the patient
    Args:
    pat: str, patient name
    opt: argparse object
    """
    pat_dir = os.path.join(opt.ds_dir, pat)
    fld_dirs = sorted(os.listdir(pat_dir), key=lambda x: int(x))
    fld_dirs = [os.path.join(pat_dir, item) for item in fld_dirs]

    mem_manager = MemoryManager(max_cap=opt.mem_cap, sampling=opt.mem_samp)

    # Now we iterate based on number of steps
    num_iter = int(np.ceil(len(fld_dirs) / opt.num_rec))

    for t in range(num_iter):
        # This is used to update the Global model
        print("\n***********************************")
        print(f"Training the {t} chunk for Patient {pat}\n\n")
        # Load the model
        model = FCN.Net(in_channels=18)
        model.to(opt.device)

        if t == 0:
            print("Loading Global Model")
            weights_filename = opt.global_mdl_dir
        else:
            weights_filename = os.path.join(
                opt.exp_dir, pat[-2:], f"mdl_pth_{t}.pth"
            )

        params = torch.load(weights_filename, map_location=opt.map_location)
        model.load_state_dict(params["state_dict"])

        # Getting new loaders
        train_loader, val_loader = load_pat_ml_boundary(
            fld_dirs[t * opt.num_rec : (t + 1) * opt.num_rec], mem_manager, opt
        )

        # Fine-tuning the model
        save_path = os.path.join(opt.exp_dir, pat[-2:], f"mdl_pth_{t+1}.pth")
        criterion = torch.nn.CrossEntropyLoss()
        criterion.to(opt.device)

        optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)
        scheduler = torch.optim.lr_scheduler.MultiStepLR(
            optimizer, milestones=[50, 75], gamma=0.2
        )
        fine_tune(
            train_loader,
            val_loader,
            model,
            opt.epochs,
            save_path,
            criterion,
            optimizer,
            opt.device,
            scheduler,
        )

        # Updating Memory based on the data of the current chunk
        mem_manager.update(
            model, fld_dirs[t * opt.num_rec : (t + 1) * opt.num_rec]
        )


def main():
    """Main function to update the models overtime.
    Usage:
    python cl.py --exp_name CL_Exp_2 --seed 2 --num_rec 1 --global_group 1
    """
    parser = ap.ArgumentParser()
    parser.add_argument("--exp_name", type=str, required=True)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--batch_size", type=int, default=256)
    parser.add_argument(
        "--epochs",
        type=int,
        default=100,
        help="number of epochs to train (default: 100)",
    )
    parser.add_argument(
        "--num_rec",
        type=int,
        default=1,
        help="number of recordings per step (default: 1 for this experiment)",
    )
    parser.add_argument(
        "--global_group",
        type=int,
        required=True,
        default=1,
        help="name of the global group, 1, 2 or 3",
    )
    opt = parser.parse_args()

    if opt.num_rec != 1:
        print("WARNING: The number of recordings for boundary is not 1")

    cuda = torch.cuda.is_available()
    opt.device = torch.device("cuda:0" if cuda else "cpu")
    print("Device: {}".format(opt.device))

    torch.manual_seed(opt.seed)
    random.seed(opt.seed)
    os.environ["PYTHONHASHSEED"] = str(opt.seed)
    np.random.seed(opt.seed)

    if cuda:
        map_location = None
        torch.cuda.manual_seed(opt.seed)
        torch.cuda.manual_seed_all(opt.seed)
        torch.backends.cudnn.deterministic = True
    else:

        def map_location(storage, loc):
            return storage

    group_1 = [6, 21, 9, 7, 4, 23, 5, 8]
    group_2 = [2, 22, 20, 18, 3, 10, 11, 12]
    group_3 = [16, 14, 19, 17, 13, 1, 24, 15]

    if opt.global_group == 1:
        group_client = group_2 + group_3
        global_mdl_dir = "Global_Gr1.pth"
    elif opt.global_group == 2:
        group_client = group_1 + group_3
        global_mdl_dir = "Global_Gr2.pth"
    elif opt.global_group == 3:
        group_client = group_1 + group_2
        global_mdl_dir = "Global_Gr3.pth"
    else:
        raise ValueError("Invalid global group")

    opt.map_location = map_location
    opt.mem_cap = 900
    opt.mem_samp = "Random"
    opt.ds_dir = "tr_val_chunks"
    opt.global_mdl_dir = os.path.join(
        "experiments_out", "global_models", global_mdl_dir
    )

    all_patients = ["chb" + str(i).zfill(2) for i in group_client]

    if not os.path.exists("experiments_out"):
        os.mkdir("experiments_out")
    opt.exp_dir = os.path.join("experiments_out", opt.exp_name)
    if not os.path.exists(opt.exp_dir):
        os.mkdir(opt.exp_dir)

    # Applying the update model function to all the patients
    for pat in all_patients:
        pat_dir = os.path.join(opt.exp_dir, pat[-2:])
        if not os.path.exists(pat_dir):
            os.mkdir(pat_dir)

        update_model(pat, opt)


if __name__ == "__main__":
    main()
