import os
import sys
import torch
import pandas as pd
import pickle
from torch.utils.data import DataLoader
from utils import FCN, chbmit_test, test

# Handle later what will happen for the "global"


def load_pat_test_data(pat_dir):
    test_dir = os.path.join(pat_dir, "test.pkl")

    with open(test_dir, "rb") as file:
        test_dataset = pickle.load(file)

    test_dataset = chbmit_test.CHB_Store_Test.CHBMITest.from_pkl(test_dataset)
    info_test = test_dataset.seg_list[:, :-1]
    test_df = pd.DataFrame(
        info_test,
        columns=["file_id", "start_time", "end_time", "true_label"],
    )
    test_loader = DataLoader(
        test_dataset, batch_size=len(test_dataset), shuffle=False
    )
    return test_loader, test_df


def eval_exp(exp_dir, ch_dir, out_dir, patients, map_location, device):
    model = FCN.Net(in_channels=18).to(device)
    criterion = torch.nn.CrossEntropyLoss()
    criterion.to(device)

    for i, pat in enumerate(patients):
        # data directories
        pat_dir = os.path.join(ch_dir, pat)
        fld_dirs = sorted(os.listdir(pat_dir), key=lambda x: int(x))
        data_dirs = [os.path.join(pat_dir, item) for item in fld_dirs]

        # Model dirs
        pat_dir = os.path.join(exp_dir, pat[-2:])
        mdl_dirs = [
            os.path.join(pat_dir, f"mdl_pth_{int(i)+1}.pth") for i in fld_dirs
        ]

        label_dir = os.path.join(out_dir, pat[-2:])
        if not os.path.exists(label_dir):
            os.mkdir(label_dir)

        for t, mdl_dir in enumerate(mdl_dirs):
            try:
                params = torch.load(mdl_dir, map_location=map_location)
            except FileNotFoundError:
                print("The specified file was not found.")
                continue
            except RuntimeError as e:
                print(f"An error occurred while loading the model: {e}")
                continue
            model.load_state_dict(params["state_dict"])

            label_dir_t = os.path.join(label_dir, str(t))
            if not os.path.exists(label_dir_t):
                os.mkdir(label_dir_t)

            for n, data_dir in enumerate(data_dirs):
                test_loader, df = load_pat_test_data(data_dir)
                out = test(test_loader, model, criterion, device, labels=True)
                df["pred_label"] = out[-1].tolist()
                save_dir = os.path.join(label_dir_t, f"{n}.csv")
                df.to_csv(save_dir)


def eval_global_exp(g_m_dir, ch_dir, out_dir, patients, map_location, device):
    model = FCN.Net(in_channels=18).to(device)
    criterion = torch.nn.CrossEntropyLoss()
    criterion.to(device)
    try:
        params = torch.load(g_m_dir, map_location=map_location)
    except FileNotFoundError:
        print("The specified file was not found.")
        return
    except RuntimeError as e:
        print(f"An error occurred while loading the model: {e}")
        return
    model.load_state_dict(params["state_dict"])
    for i, pat in enumerate(patients):
        # data directories
        pat_dir = os.path.join(ch_dir, pat)
        fld_dirs = sorted(os.listdir(pat_dir), key=lambda x: int(x))
        data_dirs = [os.path.join(pat_dir, item) for item in fld_dirs]

        label_dir = os.path.join(out_dir, pat[-2:])
        if not os.path.exists(label_dir):
            os.mkdir(label_dir)

        label_dir_t = os.path.join(label_dir, "0")
        if not os.path.exists(label_dir_t):
            os.mkdir(label_dir_t)

        for n, data_dir in enumerate(data_dirs):
            test_loader, df = load_pat_test_data(data_dir)
            out = test(test_loader, model, criterion, device, labels=True)
            df["pred_label"] = out[-1].tolist()
            save_dir = os.path.join(label_dir_t, f"{n}.csv")
            df.to_csv(save_dir)


def main():
    if "--quiet" in sys.argv or "-q" in sys.argv:

        class Quiet:
            def write(self, *args, **kwargs):
                pass

        sys.stdout = Quiet()  # Prevents the script from printing

    cuda = torch.cuda.is_available()
    device = torch.device("cuda:0" if cuda else "cpu")
    print("Device: {}".format(device))

    torch.manual_seed(0)
    if cuda:
        torch.backends.cudnn.deterministic = True
        torch.cuda.manual_seed(0)
        map_location = None
    else:

        def map_location(storage, loc):
            return storage

    exps_name = ["Global", "CL", "Base", "OS", "Hist", "Boundary"]
    groups = [1, 2, 3]
    seeds = [0, 1, 2]

    group_1 = [6, 21, 9, 7, 4, 23, 5, 8]
    group_2 = [2, 22, 20, 18, 3, 10, 11, 12]
    group_3 = [16, 14, 19, 17, 13, 1, 24, 15]

    out_folder = "results"
    if not os.path.exists(out_folder):
        os.mkdir(out_folder)

    ch_dir = "test_chunks"
    base_exp_dir = "experiments_out"
    all_patients = ["chb" + str(i).zfill(2) for i in range(1, 25)]

    # Generating csv files for each experiment
    for exp in exps_name:
        for gr in groups:
            if gr == 1:
                group_client = group_2 + group_3
                global_mdl_name = "Global_Gr1.pth"
            elif gr == 2:
                group_client = group_1 + group_3
                global_mdl_name = "Global_Gr2.pth"
            elif gr == 3:
                group_client = group_1 + group_2
                global_mdl_name = "Global_Gr3.pth"
            patients = [all_patients[i - 1] for i in group_client]

            for s in seeds:
                if exp == "Global":
                    global_mdl_dir = os.path.join(
                        "experiments_out", "global_models", global_mdl_name
                    )
                    out_exp = os.path.join(out_folder, global_mdl_name[:-4])
                    if not os.path.exists(out_exp):
                        os.mkdir(out_exp)
                    print("Global model is being evaluated")
                    eval_global_exp(
                        global_mdl_dir,
                        ch_dir,
                        out_exp,
                        patients,
                        map_location,
                        device,
                    )
                else:
                    exp_name = f"{exp}_Exp_Gr{gr}_S{s}"
                    exp_dir = os.path.join(base_exp_dir, exp_name)
                    if not os.path.exists(exp_dir):
                        print(f"Experiment {exp_name} does not exist")
                        continue
                    print(f"Experiment {exp_name} is being evaluated")
                    out_exp = os.path.join(out_folder, exp_name)
                    if not os.path.exists(out_exp):
                        os.mkdir(out_exp)

                    eval_exp(
                        exp_dir,
                        ch_dir,
                        out_exp,
                        patients,
                        map_location,
                        device,
                    )


if __name__ == "__main__":
    main()
