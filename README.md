# Personalized Online Seizure Detection
This repository contains the code for "Personalized Online Seizure Detection". Please find the paper [here](https://infoscience.epfl.ch/record/310693?ln=en&v=pdf).
![Alt text](images/summary.png)

## Download and unzip the Dataset
```
wget https://physionet.org/static/published-projects/chbmit/chb-mit-scalp-eeg-database-1.0.0.zip
unzip -q chb-mit-scalp-eeg-database-1.0.0.zip
```
There are incosistencies in the summary file of two patients:
1) chb12-summary.txt line 173: channel should be O1 instead of 01
2) chb24-summary.txt id of the seizure should be added to each seizure

For a quick start, you can use the following commands to fix the inconsistencies:
```
sed -i '173s/01/O1/' chb-mit-scalp-eeg-database-1.0.0/chb12/chb12-summary.txt
cp chb-mit-scalp-eeg-database-1.0.0/chb24/chb24-summary.txt chb-mit-scalp-eeg-database-1.0.0/chb24/chb24-summary.txt.bak
chmod +w chb-mit-scalp-eeg-database-1.0.0/chb24/chb24-summary.txt
cp chb24-summary.txt chb-mit-scalp-eeg-database-1.0.0/chb24/chb24-summary.txt
```

## Installing and setting up the environment
Python 3.10, Pytorch 1.12.1, and other common packages listed in `requirements.txt`
```
conda create -n seizure python=3.10
conda activate seizure
pip install -r requirements.txt
```

## Generating Data chunks
For faster loading of the data, we can generate chunks of the data and save them in a pickle file. This can be done using the following command:
```
python generate_data_chunks.py -q
```

## Training the models
To train the models, you can use the following command:
```
python train.py -q
```

## Testing the models
First, you need to generate the csv files for the test data using the following command:
```
python eval.py -q
```
Then, you can use the following command to analyse the resutls:
```
python analyse_results.py
```

## Citation
If you use this repository, please cite the following paper:

Shahbazinia, A., Ponzina, F., Miranda Calero, J. A., Dan, J., Ansaloni, G., & Atienza Alonso, D. (2024, April). Resource-Efficient Continual Learning for Personalized Online Seizure Detection. In 46th Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC).

