import numpy as np
import torch
import os
import pickle
from utils import chbmit


class MemoryManager:
    """Memory Manager for the data chunks"""

    def __init__(self, max_cap, sampling="MaxLoss", mem_type="None") -> None:
        """Initializes the Memory Manager

        Args:
                max_cap (int): maximum capacity of the memory
                sampling (str, optional): sampling method (MaxLoss or Random).
                Defaults to 'MaxLoss'.
                mem_type (str, optional): type of the memory (Tr, Val, None).
        """
        self.max_cap = max_cap
        self.sei_mem = self.Memory(
            cap=max_cap // 2, max_cap=max_cap, sampling=sampling, regular=False
        )
        self.nonsei_mem = self.Memory(
            cap=max_cap // 2, sampling=sampling, max_cap=max_cap
        )
        self.mem_type = mem_type

    def is_ready(self):  # Used for first chunk
        if self.sei_mem.usage == 0 or self.nonsei_mem == 0:
            return False
        return True

    def update(self, model, data_path) -> None:
        """Updates Memory (removing unnecessary data and adding new data)
        Args:
                model (nn.Module): the model that is going to determine
                        data importance
                data_path (str): directory of the data chunks
        """
        train_dataset, val_dataset = self.get_new_data(data_path)

        if self.mem_type == "Tr":
            sei_info = train_dataset.get_all_seizures()
            nonsei_info = train_dataset.get_all_Nonseizures()
        elif self.mem_type == "Val":
            sei_info = val_dataset.get_all_seizures()
            nonsei_info = val_dataset.get_all_Nonseizures()
        else:  # Merging train and validation (None)
            train_sei_info = train_dataset.get_all_seizures()
            train_nonsei_info = train_dataset.get_all_Nonseizures()

            val_sei_info = val_dataset.get_all_seizures()
            val_nonsei_info = val_dataset.get_all_Nonseizures()
            # Merging train and validation data
            sei_info = []
            assert len(train_sei_info) == len(
                val_sei_info
            ), "Train and Val info size mismatch"
            for data_tr, data_val in zip(train_sei_info, val_sei_info):
                sei_info.append(np.concatenate((data_tr, data_val)))

            nonsei_info = []
            assert len(train_nonsei_info) == len(
                val_nonsei_info
            ), "Train and Val info size mismatch"
            for data_tr, data_val in zip(train_nonsei_info, val_nonsei_info):
                nonsei_info.append(np.concatenate((data_tr, data_val)))

        if len(sei_info[0]):  # Skipping if the file is nonseizure
            # Adding seizures
            self.sei_mem.add_data(model, sei_info)
        # Adding nonseizures
        self.nonsei_mem.set_cap(self.max_cap - self.sei_mem.get_usage())
        self.nonsei_mem.add_data(model, nonsei_info)

    def get_data(self):
        sei_data, sei_seg = self.sei_mem.get_data_mem()
        nonsei_data, nonsei_seg = self.nonsei_mem.get_data_mem()

        sei_info = [sei_data, np.full((len(sei_data)), 1), sei_seg]
        nonsei_info = [nonsei_data, np.full((len(nonsei_data)), 0), nonsei_seg]
        return sei_info, nonsei_info

    @staticmethod
    def get_new_data(rec_dirs):
        assert isinstance(rec_dirs, list), "fld_dir should be a list"

        def get_dataset(fld_dir):
            train_dir = os.path.join(fld_dir, "train.pkl")
            val_dir = os.path.join(fld_dir, "val.pkl")

            with open(train_dir, "rb") as file:
                train_dataset = pickle.load(file)
            with open(val_dir, "rb") as file:
                val_dataset = pickle.load(file)
            print("Loaded:\t", fld_dir)
            train_dataset = chbmit.CHB_Store.CHBMIT.from_pkl(train_dataset)
            val_dataset = chbmit.CHB_Store.CHBMIT.from_pkl(val_dataset)

            return train_dataset, val_dataset

        train_dataset, val_dataset = get_dataset(rec_dirs[0])

        if len(rec_dirs) > 1:
            for rec_dir in rec_dirs[1:]:
                temp_train_dataset, temp_val_dataset = get_dataset(rec_dir)
                # Seizure part
                train_dataset.extend_dataset(
                    *temp_train_dataset.get_all_seizures()
                )
                val_dataset.extend_dataset(
                    *temp_val_dataset.get_all_seizures()
                )
                # Non-Seizure part
                train_dataset.extend_dataset(
                    *temp_train_dataset.get_all_Nonseizures()
                )
                val_dataset.extend_dataset(
                    *temp_val_dataset.get_all_Nonseizures()
                )

        return train_dataset, val_dataset

    class Memory:
        def __init__(self, cap, max_cap, sampling, regular=True) -> None:
            self.cap = cap
            self.max_cap = max_cap
            self.sampling = sampling
            self.num_ch = 0
            self.usage = 0
            self.regular = regular
            self.data = np.empty((max_cap, 18, 1024))
            self.label = np.empty((max_cap,), dtype=int)
            self.seg = np.empty((max_cap, 5))
            self.ch_ids = np.ones((max_cap,), dtype=int) * 1000

        def set_cap(self, cap) -> None:
            self.cap = cap

        def get_usage(self) -> int:
            return self.usage

        def get_data_mem(self) -> None:
            assert self.usage <= self.cap, "Usage is more than the capacity!!"
            return self.data[: self.usage], self.seg[: self.usage]

        def reduce_to_new_size(self, mem_data_score, cap_per_ch) -> None:
            """Reduces the size of the memory chunks

            Args:
                    mem_data_score (_type_): Scores gotten from the model
                    cap_per_ch (_type_): capacity Per chunk
            """
            # print(self.ch_ids)
            # Double checking that self.ch_ids are sorted
            assert np.all(
                np.diff(self.ch_ids[: self.usage]) >= 0
            ), "self.ch_ids is not sorted"
            if not self.regular:
                print("Removing seizures from memory")

            # Making segments of ch_ids
            segments = {}
            current_id = None
            segment_indices = []
            # print(self.usage)
            for idx, unique_id in enumerate(self.ch_ids[: self.usage]):
                if unique_id != current_id:
                    if current_id is not None:
                        segments[current_id] = segment_indices
                    current_id = unique_id
                    segment_indices = [idx]
                else:
                    segment_indices.append(idx)
            # Add the last segment
            if current_id is not None:
                segments[current_id] = segment_indices
            # for unique_id, indices in segments.items():
            # 	print(f"ID {unique_id}: {indices}")

            # updating per chunk
            ## Double check, but these parts can be merged
            # For irregular memory
            if not self.regular:
                usage = 0
                for unique_id, indices in segments.items():
                    seg_score = mem_data_score[indices]
                    sel_indices = seg_score.argsort()[:cap_per_ch]
                    if len(indices) < cap_per_ch:
                        new_rng = range(usage, usage + len(indices))
                        usage += len(indices)
                    else:
                        new_rng = range(usage, usage + cap_per_ch)
                        usage += cap_per_ch

                    final_ind = [indices[i] for i in sel_indices]
                    self.data[new_rng] = self.data[final_ind]
                    self.label[new_rng] = self.label[final_ind]
                    self.ch_ids[new_rng] = self.ch_ids[final_ind]
                self.usage = usage
            # For normal scenario
            else:
                for unique_id, indices in segments.items():
                    seg_score = mem_data_score[indices]
                    sel_indices = seg_score.argsort()[:cap_per_ch]
                    new_rng = range(
                        unique_id * cap_per_ch, (unique_id + 1) * cap_per_ch
                    )
                    final_ind = [indices[i] for i in sel_indices]
                    self.data[new_rng] = self.data[final_ind]
                    self.label[new_rng] = self.label[final_ind]
                    self.ch_ids[new_rng] = self.ch_ids[final_ind]
                assert (self.num_ch - 1) * cap_per_ch == new_rng[
                    -1
                ] + 1, "The size does not match"
                self.usage = cap_per_ch * (self.num_ch - 1)

        def expand_mem(self, new_info_score, new_info) -> None:
            # Selecting the most important samples
            if self.sampling == "Random":
                indices = np.random.choice(
                    new_info_score.shape[0], new_info_score.shape[0]
                )
            elif self.sampling == "MaxLoss":
                indices = new_info_score.argsort()
            else:
                raise ValueError("Sampling method is not defined")

            # For the case that first chunk_size < mem.size
            space = min(self.cap - self.usage, len(new_info_score))
            # In these experiments it should not happen, becuase num_ch < 200
            assert space, "There is not enought space in memory"

            self.data[self.usage : self.usage + space] = new_info[0][
                indices[:space]
            ]
            self.label[self.usage : self.usage + space] = new_info[1][
                indices[:space]
            ]
            self.seg[self.usage : self.usage + space] = new_info[2][
                indices[:space]
            ]
            self.ch_ids[self.usage : self.usage + space] = np.ones(
                space, dtype=int
            ) * (self.num_ch - 1)
            self.usage += space

        def add_data(self, model, new_info) -> None:
            """Adds new data to memory
                    Note that the there is another argument which is in the
                    self.expand_mem and if sampling is random the scores
                    are not going to be used.
            Args:
                    model (pytorch model): model to select the samples
                    new_info (np.ndarray, optional): Data of new chunk.
            """
            self.num_ch += 1
            if not self.usage:  # First Chunk
                new_info_score = self.get_scores(
                    new_info[0], new_info[1], model
                )
                self.expand_mem(new_info_score, new_info)
                return

            new_info_score = self.get_scores(new_info[0], new_info[1], model)
            mem_data_score = self.get_scores(
                self.data[: self.usage], self.label[: self.usage], model
            )

            # Limiting the chunks if needed
            if len(new_info_score) + len(mem_data_score) > self.cap:
                # remove some data
                cap_per_ch = self.cap // self.num_ch
                self.reduce_to_new_size(mem_data_score, cap_per_ch)
                # Warning: could be improved for seizures' memory when
                # when we have a lot of seizures.
            # print('usage: ', self.usage)
            self.expand_mem(new_info_score, new_info)
            # print('usage: ', self.usage)

        @staticmethod
        def get_scores(data, labels, model) -> np.array:
            """Returns score for the input data

            Args:
                    data (np.ndarray): data with shape size*18
                    model (pytorch model): a model that the scores will
                            calculated based on that

            Returns:
                    np.array: the score with size*1 shape
            """
            data = torch.from_numpy(data)
            with torch.no_grad():
                output = model(data.type(torch.cuda.FloatTensor))
                probs = output.cpu().numpy()
            prob_gt = np.array(
                [probs[j, labels[j]] for j in range(probs.shape[0])]
            )

            return prob_gt
