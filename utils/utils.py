import time
import torch


def fine_tune(
    train_loader,
    val_loader,
    model,
    epochs,
    pth_adr,
    criterion,
    optimizer,
    device,
    scheduler,
):
    start = time.time()
    best_metric = 1
    metric_name = "F1"
    f1_available = False
    # Rarely happens but for the cases that f1 is below 1.
    first_f1 = True
    for epoch in range(1, epochs + 1):
        train_loss = train_one_epoch(
            train_loader, model, criterion, optimizer, device
        )
        scheduler.step()
        (
            val_Sensitivity,
            val_Specificity,
            val_Precision,
            val_Accuracy,
            val_loss,
            _,
        ) = test(val_loader, model, criterion, device)
        (
            train_Sensitivity,
            train_Specificity,
            train_Precision,
            train_Accuracy,
            _,
            _,
        ) = test(train_loader, model, criterion, device)
        print(
            f"Epoch {epoch}. Val Sensitivity = {val_Sensitivity:.3f} , "
            f"Val Specificity = {val_Specificity:.3f} , "
            f"Val Precision = {val_Precision:.3f}, "
            f"Val Accuracy = {val_Accuracy:.3f} , "
            f"Val Loss = {val_loss:.6f}"
        )
        print(
            f"Epoch {epoch}. Train Sensitivity = {train_Sensitivity:.3f} , "
            f"Train Specificity = {train_Specificity:.3f} , "
            f"Train Precision = {train_Precision:.3f}, "
            f"Train Accuracy = {train_Accuracy:.3f} , "
            f"Train Loss = {train_loss:.6f} , "
        )
        if val_Precision != 0 and val_Sensitivity != 0:
            # Caution this update method is only valid here,
            # because we are sure for data w/ seizure we always have
            # non zero Sens and Prec.
            val_metric = 2 / (1 / val_Precision + 1 / val_Sensitivity)
            if first_f1:
                first_f1 = False
                best_metric = val_metric
            is_best = val_metric >= best_metric
            f1_available = True
        elif not f1_available:
            # For the scenarios that we don't have the F1
            # (like data w/o Seizure), We are going to use the loss
            print("Warning: F1 Not available.\n")
            val_metric = val_loss
            is_best = val_metric <= best_metric
        else:
            is_best = False
        print("\n")
        if is_best:
            # if True:
            best_metric = val_metric
            torch.save(
                {
                    "epoch": epoch + 1,
                    "state_dict": model.state_dict(),
                    "best_" + metric_name: val_metric,
                    "optimizer": optimizer.state_dict(),
                    "criterion": criterion,
                },
                pth_adr,
            )

    print("Elapsed time in minutes = {}".format((time.time() - start) / 60))
    print("best " + metric_name + " on validation: ", best_metric)
    print("\n\n\n")


def train_one_epoch(
    train_loader,
    model,
    criterion,
    optimizer,
    device,
):
    model.train()
    train_loss = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data.type(torch.cuda.FloatTensor))
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()
        train_loss += criterion(output, target).item()
    train_loss /= len(train_loader.sampler)
    return train_loss


def accuracy(output, target):
    with torch.no_grad():
        _, pred = output.topk(1, 1, True, True)
        pred = pred.t()
        (
            TRUE_POSITIVE,
            TRUE_NEGATIVE,
            FALSE_POSITIVE,
            FALSE_NEGATIVE,
            CORRECT,
        ) = (0, 0, 0, 0, 0)
        for predicted, label in zip(pred[0], target):
            if predicted == 0 and label == 0:
                TRUE_NEGATIVE = TRUE_NEGATIVE + 1
            if predicted == 0 and label == 1:
                FALSE_NEGATIVE = FALSE_NEGATIVE + 1
            if predicted == 1 and label == 0:
                FALSE_POSITIVE = FALSE_POSITIVE + 1
            if predicted == 1 and label == 1:
                TRUE_POSITIVE = TRUE_POSITIVE + 1
        CORRECT = TRUE_POSITIVE + TRUE_NEGATIVE
        return (
            TRUE_POSITIVE,
            TRUE_NEGATIVE,
            FALSE_POSITIVE,
            FALSE_NEGATIVE,
            CORRECT,
        )


def test(test_loader, model, criterion, device, labels=False):
    model.eval()
    test_loss = 0
    TRUE_POSITIVE, TRUE_NEGATIVE, FALSE_POSITIVE, FALSE_NEGATIVE, CORRECT = (
        0,
        0,
        0,
        0,
        0,
    )
    if labels:
        assert (
            len(test_loader) == 1
        ), "This is for gathering lables for 1 batch"
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data.type(torch.cuda.FloatTensor))
            test_loss += criterion(output, target).item()
            tp, tn, fp, fn, c = accuracy(output.data, target.data)
            _, pred = output.data.topk(1, 1, True, True)
            pred = pred.t()
            TRUE_POSITIVE += tp
            TRUE_NEGATIVE += tn
            FALSE_POSITIVE += fp
            FALSE_NEGATIVE += fn
            CORRECT += c
    test_loss /= len(test_loader.sampler)
    Specificity = 100.0 * TRUE_NEGATIVE / (TRUE_NEGATIVE + FALSE_POSITIVE)
    if (TRUE_POSITIVE + FALSE_NEGATIVE) > 0:
        Sensitivity = 100.0 * TRUE_POSITIVE / (TRUE_POSITIVE + FALSE_NEGATIVE)
    else:
        Sensitivity = 0.0
    if (TRUE_POSITIVE + FALSE_POSITIVE) > 0:
        Precision = 100.0 * TRUE_POSITIVE / (TRUE_POSITIVE + FALSE_POSITIVE)
    else:
        Precision = 0.0
    Accuracy = 100.0 * CORRECT / len(test_loader.sampler)

    if labels:
        _, pred = output.topk(1, 1, True, True)
        pred = pred.t()
        return (
            Sensitivity,
            Specificity,
            Precision,
            Accuracy,
            test_loss,
            FALSE_POSITIVE,
            pred[0],
        )

    else:
        return (
            Sensitivity,
            Specificity,
            Precision,
            Accuracy,
            test_loss,
            FALSE_POSITIVE,
        )
