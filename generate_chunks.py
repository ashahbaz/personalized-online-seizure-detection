"""
Creates files with spliting train and validation - test for faster loading and
also fixed data split for fair comparison between groups and scenarios
"""

import os
import sys
import random
import numpy as np
from utils.chbmit import CHB_Store
from utils.chbmit_test import CHB_Store_Test

if "--quiet" in sys.argv or "-q" in sys.argv:

    class Quiet:
        def write(self, *args, **kwargs):
            pass

    sys.stdout = Quiet()  # Prevents the script from printing

random.seed(0)
os.environ["PYTHONHASHSEED"] = str(0)
np.random.seed(0)


def generate_dirs(ds_dir, all_patients):
    if not os.path.exists(ds_dir):
        os.mkdir(ds_dir)

    for pat in all_patients:
        pat_dir = os.path.join(ds_dir, pat)
        if not os.path.exists(pat_dir):
            os.mkdir(pat_dir)


data_path = "chb-mit-scalp-eeg-database-1.0.0/"
if not os.path.exists(data_path):
    raise ValueError("Please download the CHB-MIT dataset")
all_patients = ["chb" + str(i).zfill(2) for i in range(1, 25)]
ds_dir = "tr_val_chunks"
generate_dirs(ds_dir, all_patients)
ratio = -10
train_ratio = 0.8
chunks = -1
window_size = 4

for patient in all_patients:
    CHB_Store(
        data_path,
        patient,
        window_size,
        ratio,
        train_ratio,
        chunks,
        last_seizure=True,
        only_last_chunk=False,
        first_last_seizure=True,
        repeating_ns=False,
        saving_seizure_all=False,
        dataset_dir=ds_dir,
    )

ds_dir = "test_chunks"
generate_dirs(ds_dir, all_patients)
window_size = 1
# %%
for patient in all_patients:
    CHB_Store_Test(
        data_path,
        patient,
        window_size,
        ratio,
        train_ratio,
        chunks,
        last_seizure=True,
        only_last_chunk=False,
        first_last_seizure=True,
        repeating_ns=False,
        saving_seizure_all=False,
        dataset_dir=ds_dir,
    )
# %%
