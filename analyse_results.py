import os
import pandas as pd
import numpy as np
from sklearn.metrics import f1_score
from scipy.signal import medfilt

from timescoring.annotations import Annotation
from timescoring import scoring

# Handle later what will happen for the "global"

FILTER_SIZE = 11


def calc_score_event_based(df, pred_lables, true_lables):
    if not isinstance(pred_lables, np.ndarray):
        pred_lables = pred_lables.to_numpy()
    if not isinstance(true_lables, np.ndarray):
        true_lables = true_lables.to_numpy()

    fs = 1

    if df["file_id"].nunique() == 1:
        ref = Annotation(true_lables, fs)
        hyp = Annotation(pred_lables, fs)

        param = scoring.EventScoring.Parameters(
            toleranceStart=30,
            toleranceEnd=60,
            minOverlap=0,
            maxEventDuration=5 * 60,
            minDurationBetweenEvents=90,
        )

        scores = scoring.EventScoring(ref, hyp, param)
        f1 = scores.f1
        fpRate = scores.fpRate

    elif df["file_id"].nunique() > 1:  # more than 1 file in chunk
        # spliting the dataframe based on file_id
        df_lst = []
        for file_id in df["file_id"].unique():
            df_lst.append(df[df["file_id"] == file_id])
        # calculating the scores for each file
        fp_lst = []
        tp_lst = []
        ref_p_lst = []  # refTrue is fn + tp
        num_samples_lst = []
        for df in df_lst:
            # getting indices of file_id
            index = df.index.tolist()
            ref = Annotation(true_lables[index], fs)
            hyp = Annotation(pred_lables[index], fs)

            param = scoring.EventScoring.Parameters(
                toleranceStart=30,
                toleranceEnd=60,
                minOverlap=0,
                maxEventDuration=5 * 60,
                minDurationBetweenEvents=90,
            )

            scores = scoring.EventScoring(ref, hyp, param)
            fp_lst.append(scores.fp)
            tp_lst.append(scores.tp)
            ref_p_lst.append(scores.refTrue)
            num_samples_lst.append(scores.numSamples)
        # calculating the final scores
        fp = sum(fp_lst)
        if sum(ref_p_lst) == 0 or (sum(tp_lst) + sum(fp_lst)) == 0:
            f1 = np.nan
        else:
            f1 = 2 * sum(tp_lst) / (sum(ref_p_lst) + sum(tp_lst) + sum(fp_lst))
        fpRate = fp / (sum(num_samples_lst) / fs / 3600 / 24)

    return f1, fpRate


def gather_results(out_exp, pat):
    pat_path = os.path.join(out_exp, pat)
    # checking for available chunks' output
    models = sorted(map(int, os.listdir(pat_path)))
    chunks = sorted(
        map(
            lambda x: int(x[:-4]),
            os.listdir(os.path.join(pat_path, str(models[-1]))),
        )
    )
    # using 'models[-1]+1' to Cover OS case
    all_scores = np.zeros((models[-1] + 1, len(chunks)))
    all_scores_smooth = np.zeros((models[-1] + 1, len(chunks)))
    all_scores_events = np.zeros((models[-1] + 1, len(chunks)))
    fprate_events = np.zeros((models[-1] + 1, len(chunks)))

    for i, model in enumerate(models):
        if i != 0 and model != models[i - 1] + 1:  # OS case
            for j in range(models[i - 1] + 1, model):  # filling the gap
                all_scores[j, :] = all_scores[models[i - 1], :]
                all_scores_smooth[j, :] = all_scores_smooth[models[i - 1], :]
                all_scores_events[j, :] = all_scores_events[models[i - 1], :]
                fprate_events[j, :] = fprate_events[models[i - 1], :]
        for chunk in chunks:
            file_path = os.path.join(pat_path, str(model), str(chunk) + ".csv")
            if os.path.exists(file_path):
                # print(file_path)
                df = pd.read_csv(file_path, index_col=0)
                f1 = f1_score(
                    df["true_label"], df["pred_label"], zero_division=0
                )
                pred_smooth = medfilt(
                    df["pred_label"], kernel_size=FILTER_SIZE
                )
                f1_smooth = f1_score(
                    df["true_label"], pred_smooth, zero_division=0
                )
                all_scores[model, chunk] = f1
                all_scores_smooth[model, chunk] = f1_smooth
                (
                    all_scores_events[model, chunk],
                    fprate_events[model, chunk],
                ) = calc_score_event_based(df, pred_smooth, df["true_label"])
    return all_scores, all_scores_smooth, all_scores_events, fprate_events


def generate_masks(results_df):
    sample_df = results_df[
        (results_df["Experiment"] == "CL") & (results_df["Seed"] == "0")
    ]["AllScoresEvents"].reset_index(drop=True)
    # Checking that df is not empty
    if sample_df.empty:
        print("WARNING: No data for Boundary experiment")
    shapes = [x.shape for x in sample_df]
    sample_df_2REC = results_df[
        (results_df["Experiment"] == "Base_2REC") & (results_df["Seed"] == "0")
    ]["AllScoresEvents"].reset_index(drop=True)
    if sample_df_2REC.empty:
        print("WARNING: No data for Base_2REC experiment")
    shapes_2REC = [x.shape for x in sample_df_2REC]

    # Masks
    fwt_nxt_sei_msk = [np.full((shape), False) for shape in shapes]
    for i in range(len(fwt_nxt_sei_msk)):
        for j in range(len(fwt_nxt_sei_msk[i]) - 1):
            if not np.all(np.isnan(sample_df[i][:, j + 1])):
                fwt_nxt_sei_msk[i][j, j + 1] = True

    fwt_nxt_msk = [np.full((shape), False) for shape in shapes]
    for i in range(len(fwt_nxt_msk)):
        for j in range(len(fwt_nxt_msk[i]) - 1):
            fwt_nxt_msk[i][j, j + 1] = True

    # Masks 2REC
    fwt_nxt_sei_msk_2REC = [np.full((shape), False) for shape in shapes_2REC]
    for i in range(len(fwt_nxt_sei_msk_2REC)):
        for j in range(len(fwt_nxt_sei_msk_2REC[i]) - 1):
            up_limit = min(2 * (j + 2), len(fwt_nxt_sei_msk_2REC[i][0]))
            for k in range(2 * (j + 1), up_limit):
                if not np.all(np.isnan(sample_df_2REC[i][:, k])):
                    fwt_nxt_sei_msk_2REC[i][j, k] = True

    fwt_nxt_msk_2REC = [np.full((shape), False) for shape in shapes_2REC]
    for i in range(len(fwt_nxt_msk_2REC)):
        for j in range(len(fwt_nxt_msk_2REC[i]) - 1):
            up_limit = min(2 * (j + 2), len(fwt_nxt_msk_2REC[i][0]))
            fwt_nxt_msk_2REC[i][j, 2 * (j + 1) : up_limit] = True
    masks = [fwt_nxt_sei_msk]
    masks_all = [fwt_nxt_msk]
    masks_2REC = [fwt_nxt_sei_msk_2REC]
    masks_all_2REC = [fwt_nxt_msk_2REC]
    return masks, masks_all, masks_2REC, masks_all_2REC


def print_final_results(results_df, exps_name, seeds):
    masks, masks_all, masks_2REC, masks_all_2REC = generate_masks(results_df)
    scenarios = ["REM", "FWT"]
    metrics = ["F1Events", "fpRateEvents"]
    columns = [a + "_" + b for a in scenarios for b in metrics]
    df_final = pd.DataFrame(columns=["Experiment"] + columns)

    def msk_handler(data, msk):
        def bwt(res_arr, fpr=False) -> float:
            rec = int(np.ceil(res_arr.shape[1] / res_arr.shape[0]))
            # print(res_arr.shape, res_arr.shape[1] / res_arr.shape[0], rec)
            bwt = 0.0
            total_num = 0

            for j in range(0, res_arr.shape[1]):  # for each column
                if res_arr[j // rec, j] == 0.0 and not fpr:
                    continue
                for i in range(
                    j + 1, res_arr.shape[0]
                ):  # for each row in that column
                    bwt += res_arr[i, j] - res_arr[j, j]
                    total_num += 1

            if total_num == 0:
                return 0.0
            return bwt / total_num

        def rem(res_arr) -> float:
            return 1.0 - np.abs(np.minimum(bwt(res_arr), 0.0))

        def rem_fpr(res_arr) -> float:
            return np.maximum(bwt(res_arr, fpr=True), 0.0)

        mean_lst = []
        for i in range(len(data)):
            if msk == -1:  # REM
                mean_lst.append(rem(np.nan_to_num(data[i])))
            elif msk == -10:  # REM FPR
                mean_lst.append(rem_fpr(np.nan_to_num(data[i])))
            else:
                mean_lst.append(np.mean(np.nan_to_num(data[i][msk[i]])))

        return np.mean(mean_lst)

    for exp_name in exps_name:
        # Checking if the experiment exists
        if exp_name not in results_df["Experiment"].unique():
            print(f"Experiment {exp_name} does not exist")
            continue
        # Creating empty row in the final dataframe
        df_final = pd.concat(
            [
                df_final,
                pd.DataFrame(
                    [
                        {
                            "Experiment": exp_name,
                            "REM_F1Events": 0.0,
                            "REM_fpRateEvents": 0.0,
                            "FWT_F1Events": 0.0,
                            "FWT_fpRateEvents": 0.0,
                        }
                    ]
                ),
            ],
            ignore_index=True,
        )
        for scenario in scenarios:
            # selecting the mask
            if scenario == "REM":
                msk = -1
                msk_all = -10
            elif scenario == "FWT":
                if exp_name == "Base" or exp_name == "OS":
                    msk = masks_2REC[0]
                    msk_all = masks_all_2REC[0]
                else:
                    msk = masks[0]
                    msk_all = masks_all[0]
            # Handling the Global case
            if exp_name == "Global" and scenario in ["FWT"]:
                df_res = results_df[
                    (results_df["Experiment"] == exp_name)
                    & (results_df["Seed"] == "0")
                ]
                all_scores_events = df_res["AllScoresEvents"].reset_index(
                    drop=True
                )
                fpRate_events = df_res["fpRateEvents"].reset_index(drop=True)
                f1_events_all = [
                    np.mean(np.nan_to_num(_list))
                    for _list in all_scores_events
                ]
                fpRate_events_all = [np.mean(_list) for _list in fpRate_events]
                df_final.loc[
                    df_final["Experiment"] == exp_name, scenario + "_F1Events"
                ] = np.round(np.mean(f1_events_all) * 100, 2)
                df_final.loc[
                    df_final["Experiment"] == exp_name,
                    scenario + "_fpRateEvents",
                ] = np.round(np.mean(fpRate_events_all), 2)
                continue
            if exp_name == "Global" and scenario in ["REM"]:
                continue  # Skipping the Global model for BWT+ and REM
            f1_events_seed = []
            fpRate_events_seed = []
            for seed in seeds:
                # Checking if the seed exists
                exp_df = results_df[results_df["Experiment"] == exp_name]
                if seed not in exp_df["Seed"].unique():
                    print(f"Seed {seed} does not exist for {exp_name}")
                    continue

                print(exp_name, scenario, seed)
                df_res = results_df[
                    (results_df["Experiment"] == exp_name)
                    & (results_df["Seed"] == seed)
                ]
                f1_events_seed.append(
                    msk_handler(
                        df_res["AllScoresEvents"].reset_index(drop=True),
                        msk,
                    )
                )
                fpRate_events_seed.append(
                    msk_handler(
                        df_res["fpRateEvents"].reset_index(drop=True),
                        msk_all,
                    )
                )
            # Storing the average results
            df_final.loc[
                df_final["Experiment"] == exp_name, scenario + "_F1Events"
            ] = np.round(np.mean(f1_events_seed) * 100, 2)
            df_final.loc[
                df_final["Experiment"] == exp_name, scenario + "_fpRateEvents"
            ] = np.round(np.mean(fpRate_events_seed), 2)

    print(df_final)


def main():
    exps_name = ["Global", "CL", "Base", "OS", "Hist", "Boundary"]
    groups = ["1", "2", "3"]
    seeds = ["0", "1", "2"]

    out_folder = "results"
    if not os.path.exists(out_folder):
        os.mkdir(out_folder)

    results_df = pd.DataFrame(
        columns=[
            "Experiment",
            "Seed",
            "Patient",
            "AllScores",
            "AllScoresSmooth",
            "AllScoresEvents",
            "fpRateEvents",
        ]
    )

    # Gathering results for all experiments
    for exp in exps_name:
        for gr in groups:
            for s in seeds:
                exp_name = f"{exp}_Exp_Gr{gr}_S{s}"
                if exp == "Global":
                    exp_name = f"Global_Gr{gr}"
                if exp == "Global" and s != "0":
                    break
                out_exp = os.path.join(out_folder, exp_name)
                if not os.path.exists(out_exp):
                    print(f"Experiment {exp_name} does not exist")
                    continue
                patients = sorted(os.listdir(out_exp))
                for pat in patients:
                    (
                        all_scores,
                        all_scores_smooth,
                        all_scores_events,
                        fprate_events,
                    ) = gather_results(out_exp, pat)

                    results_df = pd.concat(
                        [
                            results_df,
                            pd.DataFrame(
                                [
                                    {
                                        "Experiment": exp,
                                        "Seed": s,
                                        "Patient": pat,
                                        "AllScores": all_scores,
                                        "AllScoresSmooth": all_scores_smooth,
                                        "AllScoresEvents": all_scores_events,
                                        "fpRateEvents": fprate_events,
                                    }
                                ]
                            ),
                        ],
                        ignore_index=True,
                    )
    print_final_results(results_df, exps_name, seeds)


if __name__ == "__main__":
    main()
